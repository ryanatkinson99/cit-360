package Week4CITHTTP;

import Week4CIT.Json;
import Week4CIT.colleges;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpExchange;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public class myhttp {
    public static void main(String[] args) throws IOException {
        HttpServer server =HttpServer.create(new InetSocketAddress(8080), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(myhttp::handleRequest);
        server.start();
    }
    private static void handleRequest(HttpExchange exchange) throws IOException {

       colleges college = new colleges();
        college.setName("Brigham Young University - Idaho");
        college.setPhone(2084963000);

        String response  = Json.ToJson(college);

        exchange.getResponseHeaders().set("Content-Type", "application/Json");
        //exchange.sendResponseHeaders(200, response.getBytes().length);
        exchange.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
