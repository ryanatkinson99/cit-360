package Week4CITHTTP;
import Week4CIT.colleges;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;

public class myJson {
    public static String ToJson(String string) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(string);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }
}
