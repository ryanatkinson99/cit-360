package Week4CITHTTP;
import java.net.*;
import java.io.*;
import java.util.*;

public class HTTP {
    public static String HTTPContent(String string){
    /*Exception introduced*/
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            /*Buffered and StringBuilder classes introduced to keep current update with strings*/
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();


        } catch (IOException e){
            System.err.println(e.toString());
        }
        return "Error Encountered";
    }

    public static Map HTTPHeaders(String string){
        try { URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getHeaderFields();

        } catch(IOException e){
            System.err.println(e.toString());
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(HTTP.HTTPContent("https://www.byui.edu"));

        Map<Integer, List<String>> m =HTTP.HTTPHeaders("https://google.com");
        for(Map.Entry<Integer, List<String>> entry: m.entrySet()){
            System.out.println("Key= " + entry.getKey() + "Values = " + entry.getValue());
        }

    }
}
