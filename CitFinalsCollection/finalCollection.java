package CitFinalsCollection;

import java.util.*;

public class finalCollection {
    public static void main(String[] arg) {

        /* Collections of a list below */
        System.out.println("------Taxable items");
        List string = new ArrayList();
        string.add("Tuition");
        string.add("Books");
        string.add("School Material");
        string.add("Parking Pass");
        string.add("Donations");
        string.add("Gas Mileage");
        System.out.println("Please review and know if you have any of this that we will be adding");

        for(Object str: string ){
            System.out.println((String) str);
        }

        /* Collections for a set below */
        System.out.println("------Fixed Tax Items");
        Set set = new TreeSet();
        set.add("Dependants");
        set.add("Federal Tax Withheld");
        set.add("State Tax Withheld");
        set.add("Donations");
        set.add("Other side earnings");
        for(Object str: set ){
            System.out.println((String) str);
        }

        /* Collections of Queue */
        System.out.println("------Dependants");
        Queue queue = new PriorityQueue();
        queue.add("schooling");
        queue.add("medical bills");
        queue.add("donations");
        queue.add("elderly");
        queue.add("people living under your roof");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }


        System.out.println("Considerable items to file for");
        Map map = new HashMap();
        map.put(0,"Charity Events");
        map.put(1," Computers");
        map.put(2," Wifi");
        map.put(3," Family vacation");
        map.put(4," crash courses");
        map.put(5," Crypto Currency");
        map.put(1," Business Tips");

        for (int i = 0; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.print(result);
        }

        System.out.println("**************************************************** " +
                "**************************************************** ");





    }
}
