package Week5CITJUnit;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;

import static org.junit.jupiter.api.Assertions.*;

class AssertTest {

    @Test
    public void AssertEqual() {
        Assert college = new Assert("Brigham Young University", 2082701984);
        assertEquals("Brigham Young University", college.getName());

    }

    @Test
    public void AssertTrue() {
        Assert True = new Assert("BYU-Idaho", 2084961234);
        assertTrue(True.AssertTrue("BYU-Idaho"), True.getName());
    }

    @Test
    public void AssertFalse() {
        Assert False = new Assert("BYU-Idaho", 2084961234);
        assertFalse(False.AssertTrue("Brigham Young Idaho"), False.getName());
    }





    @Test
    public void AssertArrayEqual() {

        String[] Output1 = {"apple", "mango", "grape"};
        String[] Output2 = {"apple", "mango", "grape"};
        assertArrayEquals(Output1, Output2);
    }

    @Test
    public void AssertNotNull() {
        Assert NotNUlL = new Assert("Brigham Young University", 2082701984);
        assertNotNull("Arizona State");

    }

    @Test
    public void AssertNull() {
        Assert NotNUlL = new Assert(null, 2082701984);
        assertNull(null);

    }


    @Test
    public void Same() {
        String A = "Brigham Young University";
        String B = "Brigham Young University";
        assertSame(A, B, "These two string are the same");
    }

    @Test
    public void NotSame() {
        String A = "Brigham Young University-Idaho";
        String B = "Brigham Young University";
        assertNotSame(A, B, "These two string are the same");
    }
}




