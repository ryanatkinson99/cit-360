package Hibernate;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class Test {
    SessionFactory factory = null;
    Session session = null;

    private static Test single_instance = null;
    private Test()
    {
        factory = hibernateUtils.getSessionFactory();
    }
    public static Test getInstance(){
        if(single_instance == null){
            single_instance = new Test();
        }
        return single_instance;
    }
    public List<clients> getClients() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.clients";
            List<clients> cs = (List<clients>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


        public clients getClients(int id){
            try{
                session = factory.openSession();
                session.getTransaction().begin();
                String sql = "from Hibernate.clients " + Integer.toString(id);
                clients c = (clients)session.createQuery(sql).getSingleResult();
                session.getTransaction().commit();
                return  c;
            } catch (Exception e) {
                e.printStackTrace();
                session.getTransaction().rollback();
                return null;
            }finally{
                session.close();
            }
        }

    }

