package Week8CITExcute;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class functionExecute {
    public static void main(String[] args){

        ExecutorService myService = Executors.newFixedThreadPool(3);


        function makeFood = new function("Double stack burger");
        makeFood.start();
        function makeFood1 = new function("Milk Shake");
        makeFood1.start();
        function makeFood2 = new function("Large Fries");
        makeFood2.start();

        myService.shutdown();

    }

}
