package CitFinalsException;

public class Main {
    static int divide(int number3, int number4) {
        if (number4 == 0) {
            throw new ArithmeticException("Your numbers entered are not able to be divided. Please dont enter 0");

        } else {
            return number3 / number4;

        }

    }
}
