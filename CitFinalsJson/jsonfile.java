package CitFinalsJson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class jsonfile {
    public static String ToJson(Clients Clients) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(Clients);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }

    public static Clients ToClients(String s){
        ObjectMapper mapper = new ObjectMapper();
        Clients Clients = null;
        try {
            Clients = mapper.readValue(s, Clients.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Clients;
    }

    public static void main(String[] args) {
        Clients Client = new Clients();
        Client.setName("Leo Atkinson");
        Client.setPhone(2082701984);

        String json = jsonfile.ToJson(Client);
        System.out.println(json);

        Clients Client2 = jsonfile.ToClients(json);
        System.out.println(Client2);
    }
}
