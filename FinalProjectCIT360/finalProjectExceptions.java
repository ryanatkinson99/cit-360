package FinalProjectCIT360;
import com.sun.jdi.event.ExceptionEvent;
import java.util.*;

import static FinalProjectCIT360.taxcalculation.addition;
import static FinalProjectCIT360.taxCalculator.divide;

public class finalProjectExceptions {

        public static void main (String[] arg) {


            /*This code gets two inputted number from the user*/

            Scanner input = new Scanner(System.in);
            System.out.println("Please enter amount on W2 for Federal with-holdings: ");
            int number1 = input.nextInt();
            int number2 = input.nextInt();
            System.out.println("The is the total of the two numbers when added: " + (addition(number1, number2)));

            System.out.println("/*   Part B of the Assignment */ " );

            System.out.println("Please enter two numbers to be divided: ");
            Scanner input1 = new Scanner(System.in);
            int number3 = input1.nextInt();
            int number4 = input1.nextInt();


            /*This is where I called my division method*/
            System.out.println("The previous numbers entered when divided results are: " + (divide(number3, number4)));



            /*Exception handling is used to catch the most specific exception possible */
            System.out.println("Please enter two numbers again to test exception handling part 'C' of assignment: ");

            try {
                Scanner input2 = new Scanner(System.in);
                int number5 = input2.nextInt();
                int number6 = input2.nextInt();
                System.out.println("The previous numbers entered when divided results are: " + (divide(number5, number6)));



            } catch (Exception e) {
                System.out.println("Do not enter a '0' or 'lettes' you will break the code. Now try again: ");
                Scanner input6 = new Scanner(System.in);
                int number9 = input6.nextInt();
                int  number10 = input6.nextInt();
                System.out.println("The previous numbers entered when divided results are: " + (divide(number9, number10)));




            } finally {

                System.out.println("Very good job!");
            }






        }



    }
