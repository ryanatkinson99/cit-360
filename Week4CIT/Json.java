package Week4CIT;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public class Json {
    public static String ToJson(colleges colleges) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(colleges);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }

    public static colleges ToColleges(String s){
        ObjectMapper mapper = new ObjectMapper();
        colleges colleges = null;
        try {
            colleges = mapper.readValue(s, colleges.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return colleges;
    }

    public static void main(String[] args) {
        colleges college = new colleges();
        college.setName("Brigham Young University - Idaho");
        college.setPhone(2084963000);

        String json = Json.ToJson(college);
        System.out.println(json);

        colleges college2 = Json.ToColleges(json);
        System.out.println(college2);
    }
}
