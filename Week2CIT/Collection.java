package Week2CIT;
import java.util.*;
public class Collection {
    public static void main(String[] arg) {

        /* Collections of a list below */
        System.out.println("------Grocery list");
        List string = new ArrayList();
        string.add("Limes");
        string.add("Cerals");
        string.add("Milk");
        string.add("Flour");
        string.add("Sugar");
        string.add("Orange Juice");
        System.out.println("Hello world");

        for(Object str: string ){
            System.out.println((String) str);
        }

        /* Collections for a set below */
        System.out.println("------This is a set");
        Set set = new TreeSet();
        set.add("Hello");
        set.add("This is");
        set.add("my");
        set.add("collection");
        set.add("List");
        for(Object str: set ){
            System.out.println((String) str);
        }

        /* Collections of Queue */
        System.out.println("------This is a Queue");
        Queue queue = new PriorityQueue();
        queue.add("Hello");
        queue.add("This is");
        queue.add("my");
        queue.add("collection");
        queue.add("List");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }


        System.out.println("This is a Map Collection");
        Map map = new HashMap();
        map.put(0,"Hello");
        map.put(1," This");
        map.put(2," is");
        map.put(3," collection");
        map.put(4," List");
        map.put(5," Paper");
        map.put(1," Jim");

        for (int i = 0; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.print(result);
        }

        /* This is a tree set */
        System.out.println("This is a tree set");
        TreeSet<String> tree = new TreeSet<String>();
        tree.add("Orange");
        tree.add("Grape");
        tree.add("Mango");
        tree.add("Lime");
        tree.add("Lemon");

        for(Object str: tree ){
            System.out.println((String) str);
        }



        System.out.println("-- List using Generics --");
        List<Positions> myList = new ArrayList<Positions>();
        myList.add(new Positions("Machael G Scott", "Regional Manager"));
        myList.add(new Positions( "Robert California",  "CEO"));
        myList.add(new Positions("Andy Bernard", "Reginal Director of Sales"));
        myList.add(new Positions( "Dwight Shrut", "Assistant to the regional Manager"));

        for (Positions newHires : myList) {
            System.out.println(newHires);
        }


        System.out.println("/**************************************************** " +
                           "***************************************************** ");


        System.out.println("List using Generics with list collection");
        System.out.println("My grocery List for this week");
        List<WeekTwoAssignment> list = new ArrayList<WeekTwoAssignment>();
        list.add(new WeekTwoAssignment("Lime"));
        list.add(new WeekTwoAssignment( "Ceral"));
        list.add(new WeekTwoAssignment("Milk"));
        list.add(new WeekTwoAssignment( "Flour"));
        list.add(new WeekTwoAssignment( "Sugar"));
        list.add(new WeekTwoAssignment( "Orange Juice"));

        for (WeekTwoAssignment grocery : list) {
            System.out.println(grocery);
        }



    }
}
